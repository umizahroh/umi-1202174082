<?php session_start(); ?>

<!DOCTYPE html>
<html>
<head>
	<title>Home</title>

	<!-- Main Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">

	<!-- Custom CSS -->
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">

	<!-- Main Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

	<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
</head>
<body>
	<div class="header">
		<nav class="navbar navbar-expand-lg navbar-white bg-white">
			<a class="navbar-brand" href="../modul4">
				<img src="assets/img/ead.png" width="100px">
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse text-body">
				<ul class="navbar-nav ml-auto">
					<?php if (isset($_SESSION['username'])) { ?>
						<li class="nav-item active">
							<span style="height: 100%; display: inline-block; vertical-align: middle;"></span>
							<a href="../modul4/cart.php">
								<img src="assets/img/shopping_cart.svg">
							</a>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<?php echo $_SESSION['username']; ?>
							</a>
							<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="./edit-profile.php">Edit Profile</a>
								<a class="dropdown-item" href="../modul4/operation/logout.php">Logout</a>
							</div>
						</li>
					<?php } else { ?>
						<li class="nav-item active">
							<button class="btn" data-toggle="modal" data-target="#loginModal">Login</button>
						</li>
						<li class="nav-item">
							<button class="btn" data-toggle="modal" data-target="#registerModal">Register</button>
						</li>
					<?php } ?>
				</ul>
			</div>
		</nav>
	</div>