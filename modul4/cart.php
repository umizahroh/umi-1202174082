<?php
include 'layout/header.php';
?>

<?php

require_once('operation/database.php');

$username = $_SESSION['username'];
$query = "SELECT * FROM cart WHERE user_id = '$username'";

$ret = mysqli_query($connect, $query);

$i = 0;
$total = 0;
while ($row = mysqli_fetch_assoc($ret)) {
    $rows[$i]['no'] = ($i+1);
    $rows[$i]['id'] = $row['id'];
    $rows[$i]['product'] = $row['product'];
    $rows[$i]['price'] = number_format($row['price'], 2);

    $total += $row['price'];

    $i++;
}
?>

<div class="container">
    <div class="row justify-content-sm-center">
        <div class="content">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Product</th>
                        <th scope="col">Price</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(isset($rows)) {  foreach ($rows as $row) { ?>
                    <tr>
                        <td align="center" style="vertical-align: middle;"><?= $row['no'] ?></td>
                        <td style="vertical-align: middle;"><?= $row['product'] ?></td>
                        <td align="right" style="vertical-align: middle;"><?= $row['price'] ?></td>
                        <td align="center" style="vertical-align: middle;">
                            <form action="operation/delete-cart.php/" method="POST">
                                <input type="hidden" name="cart_id" value="<?= $row['id']; ?>">
                                <button type="submit" class="btn btn-danger">
                                    <span class="btn-label">
                                        <i class="fas fa-times"></i>
                                    </span>
                                </button>
                            </form>
                        </td>
                    </tr>
                    <?php } } ?>
                    <tr>
                        <td colspan="2" align="right"><b>Total</b></td>
                        <td align="right"><b><?= number_format($total, 2) ?></b></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php
include 'layout/footer.php';
?>