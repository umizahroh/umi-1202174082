<?php
include 'layout/header.php';
?>

<div class="container">
	<div class="row justify-content-sm-center">
		<div class="col-sm-9 banner">
			<div class="banner-content">
				<h1>Hello Coders</h1>
				<p>Welcome to our store, please take a look for the products you might buy</p>
			</div>
		</div>
	</div>
	<div class="row justify-content-sm-center products">
		<div class="col-sm-3">
			<div class="card">
				<img src="https://www.dicoding.com/images/original/academy/web_fundamental_logo_030519090933.jpg" class="card-img-top" alt="Basic Web Programming">
				<div class="card-body">
					<h5 class="card-title">Learning Basic Web Programming</h5>
					<h6>Rp. 210.000,-</h6>
					<p class="card-text">This module is perfect for those of you who are beginners and want to learn how to become a web developer. This module combines 2 modules into 1, i.e. html css class, and JavaScript module.</p>
				</div>
				<div class="card-footer">
					<small class="text-muted">
						<form action="operation/buy.php" method="POST">
							<input type="hidden" name="product" value="Learning Basic Web Programming">
							<input type="hidden" name="price" value="210000">
							<input type="submit" class="btn btn-fill col-sm-12" value="Buy">
						</form>
					</small>
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="card">
				<img src="https://www.dicoding.com/images/original/academy/java_fundamental_logo_080119141920.jpg" class="card-img-top" alt="Java">
				<div class="card-body">
					<h5 class="card-title">Starting Programming in Java</h5>
					<h6>Rp. 150.000,-</h6>
					<p class="card-text">This module was designed by contributors, and has been reviewed by Dr. Ir. Inggriani Liem (ITB / IA TOKI / BEBRAS NBO), and adapted for those of you who want to learn the concept of Object-Oriented Programming (PBO) and learn the structure of the Java language in general.</p>
				</div>
				<div class="card-footer">
					<small class="text-muted">
						<form action="operation/buy.php" method="POST">
							<input type="hidden" name="product" value="Starting Programming in Java">
							<input type="hidden" name="price" value="150000">
							<input type="submit" class="btn btn-fill col-sm-12" value="Buy">
						</form>
					</small>
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="card">
				<img src="https://www.dicoding.com/images/original/academy/memulai_pemrograman_dengan_python_logo_090719134021.jpg" class="card-img-top" alt="Python">
				<div class="card-body">
					<h5 class="card-title">Starting Programming in Python</h5>
					<h6>Rp. 200.000,-</h6>
					<p class="card-text">Whoever you are, Python is interesting to learn. Python language is a must for you who want to learn the basics of scripting and data processing or Machine Learning.</p>
				</div>
				<div class="card-footer">
					<small class="text-muted">
						<form action="operation/buy.php" method="POST">
							<input type="hidden" name="product" value="Starting Programming in Python">
							<input type="hidden" name="price" value="200000">
							<input type="submit" class="btn btn-fill col-sm-12" value="Buy">
						</form>
					</small>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" tabindex="-1" role="dialog" id="loginModal">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<form action="operation/login.php" method="POST" id="formLogin">
					<div class="modal-header">
						<h5 class="modal-title">Login</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="reg-email">Email address</label>
							<input type="email" class="form-control" name="email" placeholder="Enter Email" required>
						</div>
						<div class="form-group">
							<label for="password">Password</label>
							<input type="password" class="form-control" name="password" placeholder="Enter Password" required>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-fill" id="btnLogin">Login</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal fade" tabindex="-1" role="dialog" id="registerModal">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<form action="operation/register.php" method="POST" id="formRegister">
					<div class="modal-header">
						<h5 class="modal-title">Register</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="email">Email address</label>
							<input type="email" class="form-control" name="email" id="email" placeholder="Enter Email" required>
						</div>
						<div class="form-group">
							<label for="username">Username</label>
							<input type="text" class="form-control" name="username" id="username" placeholder="Enter Username" required>
						</div>
						<div class="form-group">
							<label for="password">Password</label>
							<input type="password" class="form-control" name="password" id="password" placeholder="Enter Password" required>
						</div>
						<div class="form-group">
							<label for="confirm-password">Confirm Password</label>
							<input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="Retype Password" required>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-fill" id="btnRegister">Register</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<?php
include 'layout/footer.php';
?>

<script>
	$(function() {
		$('#btnRegister').click(function() {
			var isFilled = true;
			$('#formRegister input').each(function() {
				if ($(this).val() == '') {
					alert('Lengkapi Data!');
					$(this).focus();
					isFilled = false;
					return false;
				}
			});

			if (!isFilled) {
				return false;
			}

			$.ajax({
				type: 'POST',
				url: 'operation/register.php',
				data: $('#formRegister').serialize(),
				dataType: 'json',
				success: function(response) {
					if (!response.status) {
						alert(response.msg);
						return false;
					}

					window.location = '../modul4';
				}
			});
		});

		$('#btnLogin').click(function() {
			var isFilled = true;
			$('#formLogin input').each(function() {
				if ($(this).val() == '') {
					alert('Lengkapi Data!');
					$(this).focus();
					isFilled = false;
					return false;
				}
			});

			if (!isFilled) {
				return false;
			}

			$.ajax({
				type: 'POST',
				url: 'operation/login.php',
				data: $('#formLogin').serialize(),
				dataType: 'json',
				success: function(response) {
					if (!response.status) {
						alert(response.msg);
						return false;
					}

					window.location = '../modul4';
				}
			});
		})
	});
</script>