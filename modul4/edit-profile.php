<?php
include 'layout/header.php';
?>

<?php

require_once('operation/database.php');

$username = $_SESSION['username'];
$query = "SELECT * FROM users WHERE username = '$username'";

$ret = mysqli_query($connect, $query);

while ($row = mysqli_fetch_assoc($ret)) {
    $rows = $row;
}

?>

<div class="container">
    <div class="row justify-content-sm-center" style="margin-top: 30px;">
        <h3>Profil</h3>
    </div>
    <div class="row justify-content-sm-center">
        <div class="col-sm-9 edit-profile">
            <form action="operation/edit-profile.php" method="POST">
                <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" name="email" placeholder="Email" value="<?= $rows['email'] ?>" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="username" class="col-sm-2 col-form-label">Username</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="username" placeholder="Username" value="<?= $rows['username']?> ">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="mobile_number" class="col-sm-2 col-form-label">Mobile Number</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="mobile_number" placeholder="Mobile Number" value="<?= $rows['mobile_number']?>">
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <label for="new_password" class="col-sm-2 col-form-label">New Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" name="new_password" placeholder="New Password">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="confirm_password" class="col-sm-2 col-form-label">Confirm Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12 text-center">
                        <button type="submit" class="btn btn-fill">Save</button>
                        <button type="reset" class="btn btn-outline-secondary">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php
include 'layout/footer.php';
?>