<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PostsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->delete();

        DB::table('posts')->insert([
            'user_id' => $this->getRandomUserId(),
            'caption' => 'Logo Daspro',
            'image' => 'daspro.png',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('posts')->insert([
            'user_id' => $this->getRandomUserId(),
            'caption' => 'Keluarga Daspro 2017',
            'image' => 'keluarga-daspro.jpg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('posts')->insert([
            'user_id' => $this->getRandomUserId(),
            'caption' => 'Keluarga Prodase',
            'image' => 'keluarga-prodase.jpg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }

    private function getRandomUserId() {
        $user = \App\User::inRandomOrder()->first();
        return $user->id;
    }
}
