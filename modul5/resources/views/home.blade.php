@extends('layouts.app')

@section('page', 'EAD Blog')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @isset($posts)
                    @foreach($posts as $post)
                        <div class="card">
                            <div class="card-header">
                                <img src="{{ asset('assets/img/' . $post->user->avatar) }}" class="avatar">
                                <span class="post-user">{{ $post->user->name }}</span>
                            </div>
                            <div class="card-body">
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <img src="{{ asset('assets/img/' . $post->image) }}" class="post-image"
                                     alt="{{ asset('assets/img/' . $post->image) }}">
                            </div>
                            <div class="card-footer">
                                <div class="col-sm-12 text-left">
                                    <span class="post-user">{{ $post->user->email }}</span>
                                </div>
                                <div class="col-sm-12 text-left">
                                    <span class="post-caption">{{ $post->caption }}</span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endisset
            </div>
        </div>
    </div>
@endsection
