<?php
	$params = $_POST;

	$i = 0;
	$bill = 0;
	foreach ($params['menu'] as $row) {
		$bill += $params['menu'][$i];

		$i++;
	}

	if ($params['member'] == 'Ya') {
		$bill = $bill - (0.1 * $bill);
	}
	
	$bill = number_format($bill, 2);

?>

<!DOCTYPE html>
<html>
<head>
	<title>Nota</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="../assets/css/bootstrap-datepicker.min.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto:300&display=swap" rel="stylesheet">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<script src="../assets/js/bootstrap-datepicker.min.js"></script>

	<style>
		body {
			font-family: 'Roboto', sans-serif;
			color: #595c63;
		}

		.middle-container {
			background-color: white;
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%, -50%);
			text-align: center;
			border: 1px solid #ededeb;
			border-radius: 10px;
			box-shadow: 0px 0px 20px 10px #f0f0ed;
			padding: 50px;
			width: 50%;
		}

		.header {
			text-align: left;
			margin-left: 100px;
			font-weight: bold;
		}
	</style>
</head>
<body>
	<div class="middle-container">
		<h1>Transaksi Pemesanan</h1>
		<span>Terimakasih Telah Berbelanja dengan Kopi Susu Duarrr!</span>
		<br><br>
		<h1>Rp. <?php echo $bill; ?>,-</h1>
		<hr>
		<div class="row">
			<div class="col-sm-4 header">
				ID
			</div>
			<div class="col-sm-4 text-left">
				<?php echo $params['order_no'] ?>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-sm-4 header">
				Nama
			</div>
			<div class="col-sm-4 text-left">
				<?php echo $params['order_name'] ?>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-sm-4 header">
				Email
			</div>
			<div class="col-sm-4 text-left">
				<?php echo $params['email'] ?>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-sm-4 header">
				Alamat
			</div>
			<div class="col-sm-4 text-left">
				<?php echo $params['order_address'] ?>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-sm-4 header">
				Member
			</div>
			<div class="col-sm-4 text-left">
				<?php echo $params['member'] ?>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-sm-4 header">
				Pembayaran
			</div>
			<div class="col-sm-4 text-left">
				<?php echo $params['payment'] ?>
			</div>
		</div>
	</div>
</body>
</html>