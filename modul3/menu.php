<?php
$params['name'] = $_GET['name'];
$params['phone'] = $_GET['phone'];
$params['date'] = $_GET['date'];
$params['driver'] = $_GET['driver'];
$params['bag'] = (isset($_GET['bag'])) ? 'Bawa' : 'Tidak Bawa';
?>

<!DOCTYPE html>
<html>
<head>
	<title>Menu</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto:300&display=swap" rel="stylesheet">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

	<style>
		body {
			font-family: 'Roboto', sans-serif;
			color: #595c63;
			margin: 0;
			overflow: hidden;
		}

		.middle-container {
			background-color: white;
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%, -50%);
			text-align: center;
			border: 1px solid #ededeb;
			border-radius: 10px;
			box-shadow: 0px 0px 20px 10px #f0f0ed;
			padding: 50px;
			width: 50%;
		}

		.middle-container label{
			text-align: right;
		}

		.left-content {
			width: 50%;
			float: left;
			text-align: center;
			padding: 30px;
			overflow: hidden;
		}

		.right-content {
			width: 50%;
			float: right;
			text-align: center;
			padding: 0px 30px 30px 30px;
			margin-top: 35px;
			height: 580px;
			overflow-y: scroll;
		}

		.title-label {
			margin-top: 18px;
		}

		.option {
			padding-left: 100px;
			text-align: left;
		}
	</style>
</head>
<body>
	<div class="left-content">
		<h1><b>Data Driver Ojol</b></h1>
		<label class="col-form-label title-label"><b>Nama</b></label><br>
		<label><?php echo $params['name'] ?></label><br>
		<label class="col-form-label title-label"><b>Nomor Telepon</b></label><br>
		<label><?php echo $params['phone'] ?></label><br>
		<label class="col-form-label title-label"><b>Tanggal</b></label><br>
		<label><?php echo $params['date'] ?></label><br>
		<label class="col-form-label title-label"><b>Asal Driver</b></label><br>
		<label><?php echo $params['driver'] ?></label><br>
		<label class="col-form-label title-label"><b>Bawa Kantong</b></label><br>
		<label><?php echo $params['bag'] ?></label><br><br>
		<a href="../modul3/form.html" class="btn btn-outline-danger"><< Kembali</a>
	</div>

	<div class="right-content">
		<h1><b>Menu</b></h1>
		<span>Pilih Menu</span>
		<br><br>
		<form action="../modul3/nota.php" method="POST">
			<div class="form-group row">
				<div class="col-sm-6 option">
					<div class="form-check">
						<input class="form-check-input" type="checkbox" value="28000" id="menu1" name="menu[]">
						<label class="form-check-label">
							Es Coklat Susu
						</label>
					</div>
				</div>
				<div class="col-sm-6">
					<span>Rp. 28.000,-</span>
				</div>
			</div>
			<hr>
			<div class="form-group row">
				<div class="col-sm-6 option">
					<div class="form-check">
						<input class="form-check-input" type="checkbox" value="18000" id="menu2" name="menu[]">
						<label class="form-check-label">
							Es Susu Matcha
						</label>
					</div>
				</div>
				<div class="col-sm-6">
					<span>Rp. 18.000,-</span>
				</div>
			</div>
			<hr>
			<div class="form-group row">
				<div class="col-sm-6 option">
					<div class="form-check">
						<input class="form-check-input" type="checkbox" value="15000" id="menu3" name="menu[]">
						<label class="form-check-label">
							Es Susu Mojicha
						</label>
					</div>
				</div>
				<div class="col-sm-6">
					<span>Rp. 15.000,-</span>
				</div>
			</div>
			<hr>
			<div class="form-group row">
				<div class="col-sm-6 option">
					<div class="form-check">
						<input class="form-check-input" type="checkbox" value="30000" id="menu4" name="menu[]">
						<label class="form-check-label">
							Es Matcha Latte
						</label>
					</div>
				</div>
				<div class="col-sm-6">
					<span>Rp. 30.000,-</span>
				</div>
			</div>
			<hr>
			<div class="form-group row">
				<div class="col-sm-6 option">
					<div class="form-check">
						<input class="form-check-input" type="checkbox" value="21000" id="menu5" name="menu[]">
						<label class="form-check-label">
							Es Taro Susu
						</label>
					</div>
				</div>
				<div class="col-sm-6">
					<span>Rp. 21.000,-</span>
				</div>
			</div>
			<hr>
			<div class="form-group row">
				<label class="col-sm-4 col-form-label" style="text-align: right;">Nomor Order</label>
				<div class="col-sm-6">
					<input type="text" name="order_no" id="order_no" class="form-control" required>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-4 col-form-label" style="text-align: right;">Nama Pemesan</label>
				<div class="col-sm-6">
					<input type="text" name="order_name" id="order_name" class="form-control" required>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-4 col-form-label" style="text-align: right;">Email</label>
				<div class="col-sm-6">
					<input type="email" name="email" id="email" class="form-control" required>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-4 col-form-label" style="text-align: right;">Alamat Order</label>
				<div class="col-sm-6">
					<input type="text" name="order_address" id="order_address" class="form-control" required>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-4 col-form-label" style="text-align: right;">Member</label>
				<div class="col-sm-6" style="text-align: left; margin-top: 5px;">
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" value="Ya" id="yes" name="member" checked required>
						<label class="form-check-label">
							Ya
						</label>
					</div>
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" value="Tidak" id="no" name="member">
						<label class="form-check-label" required>
							Tidak
						</label>
					</div>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-4 col-form-label" style="text-align: right;">Metode Pembayaran</label>
				<div class="col-sm-6">
					<select name="payment" id="payment" class="form-control" required>
						<option value="">--Pilih Metode Pembayaran--</option>
						<option value="Cash">Cash</option>
						<option value="E-Money (OVO/Gopay)">E-Money (OVO/Gopay)</option>
						<option value="Credit Card">Credit Card</option>
						<option value="Lainnya">Lainnya</option>
					</select>
				</div>
			</div>
			<br>
			<div class="col-sm-12">
				<input type="submit" id="btnSubmit" class="btn btn-info" value="Cetak Struk">
			</div>
		</form>
	</div>
</body>
</html>

<script type="text/javascript">
	$(function() {
		var isFilled = true;
		$('#btnSubmit').click(function() {
			if ($('input[type="checkbox"]:checked').length < 1) {
				alert('Silakan Pilih Menu!');
				return false;
			}

			$('input').filter('[required]').each(function() {
				if ($(this).val() === '') {
					isFilled = false;
					return false;
				}
			});

			if (isFilled) {
				if(!confirm('Apa Anda Yakin?')) return false;
			}
		});
	});
</script>