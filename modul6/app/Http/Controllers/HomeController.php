<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Post;
use App\PostComments;
use App\User;
use Auth;
use File;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user_id = Auth::user()->id;

        $data['posts'] = Post::with(['user', 'comments.user'])->orderBy('created_at', 'desc')->get();
        return view('home', $data);
    }

    public function detail_post($id)
    {
        $data['post'] = Post::with(['user', 'comments.user'])->where('id', $id)->first();

        return view('detail-post', $data);
    }

    public function profile($id)
    {
        $data['user'] = User::withCount('post')->where('id', $id)->first();
        return view('profile', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create-post');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('image');
        $file_name = $file->getClientOriginalName();
        $file->move(public_path('/assets/img'), $file_name);

        $post = Post::create(
            array(
                'user_id' => Auth::user()->id,
                'caption' => $request->caption,
                'image' => $file_name,
                'created_by' => Carbon::now(),
                'updated_by' => Carbon::now(),
            )
        );

        return redirect()->route('home');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['user'] = Auth::user();

        return view('edit-profile', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->title = $request->title;
        $user->description = $request->description;
        $user->url = $request->url;
        $user->updated_at = Carbon::now();

        if ($request->avatar != null) {
            $file = $request->file('avatar');
            $file_name = $file->getClientOriginalName();
            $file->move(public_path('/assets/img'), $file_name);

            $user->avatar = $file_name;
        }

        $user->save();
        return redirect('/profile/' . $id);
    }

    public function comment(Request $request)
    {
        $comment = PostComments::create([
            'user_id' => Auth::user()->id,
            'post_id' => $request->post_id,
            'comment' => $request->comment,
            'created_by' => Carbon::now(),
            'updated_by' => Carbon::now(),
        ]);

        return back();
    }

    public function like($id)
    {
        $post = Post::find($id);

        $likes = $post->likes + 1;

        $post->updated_at = Carbon::now();
        $post->likes = $likes;

        $post->save();
        return back();
    }
}
