@extends('layouts.app')

@section('page', 'EAD Blog')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @isset($posts)
                    @foreach($posts as $post)
                        <div class="card">
                            <div class="card-header">
                                <a href="{{ url('profile/' . $post->user->id) }}">
                                    <img src="{{ asset('assets/img/' . $post->user->avatar) }}" class="avatar">
                                </a>
                                <a href="{{ url('profile/' . $post->user->id) }}"
                                   style="text-decoration: none; color: black">
                                    <span class="post-user">{{ $post->user->name }}</span>
                                </a>
                            </div>
                            <div class="card-body">
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <a href="{{ url('detail/' . $post->id) }}">
                                    <img src="{{ asset('assets/img/' . $post->image) }}" class="post-image"
                                         alt="{{ asset('assets/img/' . $post->image) }}">
                                </a>
                            </div>
                            <div class="card-footer">
                                <a href="{{ url('like/' . $post->id) }}" style="text-decoration: none; color: black">
                                    <i class="far fa-heart fa-2x" style="padding: 10px"></i>
                                </a>
                                <i class="far fa-comment fa-2x" style="padding: 10px"></i>
                                <div class="col-sm-12 text-left">
                                    <span class="post-user">{{ $post->likes }} Likes</span>
                                </div>
                                <div class="col-sm-12 text-left">
                                    <a href="{{ url('profile/' . $post->user->id) }}"
                                       style="text-decoration: none; color: black">
                                        <span class="post-user">{{ $post->user->email }}</span>
                                    </a>
                                    <span class="post-caption">{{ $post->caption }}</span>
                                </div>
                                @isset($post->comments)
                                    @foreach($post->comments as $comment)
                                        <div class="col-sm-12 text-left">
                                            <span class="post-user">{{ $comment->user->email }}</span>
                                            <span class="post-caption">{{ $comment->comment }}</span>
                                        </div>
                                    @endforeach
                                @endisset
                                <div class="col-sm-12">
                                    <form action="{{ url('comment') }}" method="POST">
                                        @csrf
                                        <input type="hidden" name="post_id" value="{{ $post->id }}">
                                        <div class="input-group mt-2">
                                            <input type="text" class="form-control" name="comment"
                                                   placeholder="Add a comment...">
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-secondary" type="submit">Post</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endisset
            </div>
        </div>
    </div>
@endsection
