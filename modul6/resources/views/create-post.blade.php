@extends('layouts.app')

@section('page', 'EAD Blog')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h2>Add New Post</h2>
            </div>
        </div>
        <div class="row justify-content-sm-center mt-5">
            <div class="col-sm-6">
                <form action="{{ url('post') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>Post Caption</label>
                        <textarea class="form-control" name="caption" id="caption"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Post Image</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="image" id="image">
                            <label class="custom-file-label" for="image">Choose file</label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Post</button>
                </form>
            </div>
        </div>
    </div>
@endsection
