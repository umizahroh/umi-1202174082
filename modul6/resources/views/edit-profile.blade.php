@extends('layouts.app')

@section('page', 'EAD Blog')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h2>Edit Profile</h2>
            </div>
        </div>
        <div class="row justify-content-sm-center mt-4">
            <div class="col-sm-6">
                <form action="{{ url('profile/' . $user->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" class="form-control" name="title" id="title" value="{{ $user->title }}">
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <input type="text" class="form-control" name="description" id="description" value="{{ $user->description }}">
                    </div>
                    <div class="form-group">
                        <label>URL</label>
                        <input type="text" class="form-control" name="url" id="url" value="{{ $user->url }}">
                    </div>
                    <div class="form-group">
                        <label>Profile Image</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="avatar" id="avatar">
                            <label class="custom-file-label" for="image">Choose file</label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Save Profile</button>
                </form>
            </div>
        </div>
    </div>
@endsection
