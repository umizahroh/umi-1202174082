@extends('layouts.app')

@section('page', 'EAD Blog')

@section('content')
    <div class="container">
        <div class="row mb-lg-5">
            <div class="col-sm-4 text-center pt-lg-5">
                <img src="{{ asset('assets/img/' . $user->avatar) }}" class="avatar-profile">
            </div>
            <div class="col-sm-8">
                <div class="row mt-4 mb-4">
                    <div class="col-sm-8 post-user" style="padding: 0">
                        {{ $user->name }}
                    </div>
                    @if(Auth::user()->id == $user->id)
                        <div class="col-sm-4 text-right">
                            <a href="{{ route('post') }}" class="text-right">Add New Post</a>
                        </div>
                    @endif
                </div>
                @if(Auth::user()->id == $user->id)
                    <div class="row">
                        <a href="{{ url('edit-profile/' . $user->id) }}">Edit Profile</a>
                    </div>
                @endif
                <div class="row mb-4">{{ $user->post_count }} Posts</div>
                <div class="row post-user">{{ $user->title }}</div>
                <div class="row">{{ $user->description }}</div>
                <div class="row">
                    <a href="{{ $user->url }}">{{ $user->url }}</a>
                </div>
            </div>
        </div>
        @foreach($user->post as $post)
            @if(($loop->iteration % 3 - 1) == 0)
                <div class="row">
                    @endif
                    <div class="col-sm-4">
                        <a href="{{ url('detail/' . $post->id) }}">
                            <img src="{{ asset('assets/img/' . $post->image) }}" class="post-image-profile"
                                 alt="{{ asset('assets/img/' . $post->image) }}">
                        </a>
                    </div>
                    @if($loop->iteration % 3 == 0)
                </div>
            @endif
        @endforeach
    </div>
    </div>
@endsection
