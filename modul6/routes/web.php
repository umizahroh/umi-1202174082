<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/detail/{id}', 'HomeController@detail_post')->name('detail');
Route::get('/profile/{id}', 'HomeController@profile')->name('profile');
Route::get('/post', 'HomeController@create')->name('post');
Route::post('/post', 'HomeController@store')->name('post');
Route::get('/edit-profile/{id}', 'HomeController@edit')->name('edit-profile');
Route::put('/profile/{id}', 'HomeController@update');
Route::post('/comment', 'HomeController@comment')->name('comment');
Route::get('/like/{id}', 'HomeController@like')->name('like');
